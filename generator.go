package main

import (
	"flag"
	"math/rand"
	"os"
	"time"
)

const symbols = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func generateString(n int) string {
	bytes := make([]byte, n)
	for i := range bytes {
		bytes[i] = symbols[rand.Intn(len(symbols))]
	}
	return string(bytes)
}

func main() {
	rand.Seed(time.Now().UnixNano())
	count := flag.Int("count", 20, "count of lines to generate")
	line_size := flag.Int("size", 10, "maxsize of line")
	outfile := flag.String("o", "output.txt", "output filename")
	flag.Parse()

	file, err := os.Create(*outfile)
	defer file.Close()

	if err != nil {
		panic(err)
	}

	for i := 0; i < *count; i++ {
		file.WriteString(generateString(*line_size))
		file.WriteString("\n")
	}
}
