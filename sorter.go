package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"runtime"
	"sort"
	"time"
)

const LF = '\n'
const preparedBufferSize = 10000

const (
	B        = iota
	KB int64 = 1 << (10 * iota)
	MB
	GB
)

func getBlockFilename(inFilename string, blockNumber int, iterationNumber int) string {
	return fmt.Sprintf("%s_iteration_%d_part_%d", inFilename, iterationNumber, blockNumber)
}

func sortAndWrite(inFilename string, blockNumber int, buffer []string) error {
	writer, err := os.Create(getBlockFilename(inFilename, blockNumber, 0))
	if err != nil {
		return err
	}
	defer writer.Close()
	sort.StringSlice(buffer).Sort()

	for _, str := range buffer {
		_, err = writer.WriteString(str)
		if err != nil {
			return err
		}
	}
	fmt.Printf("block %d sorted and written\n", blockNumber)
	return nil
}

func mergeFiles(filename1, filename2, output string) error {
	reader1, err := os.Open(filename1)
	if err != nil {
		return err
	}
	reader2, err := os.Open(filename2)
	if err != nil {
		return err
	}
	writer, err := os.Create(output)
	if err != nil {
		return err
	}

	defer func() {
		reader1.Close()
		reader2.Close()
		writer.Close()
		os.Remove(filename1)
		os.Remove(filename2)
	}()

	bufReader1 := bufio.NewReader(reader1)
	bufReader2 := bufio.NewReader(reader2)
	line1, err := bufReader1.ReadString(LF)
	if err != nil {
		_, err = bufReader2.WriteTo(writer)
		return err
	}
	line2, err := bufReader2.ReadString(LF)
	if err != nil {
		_, err = bufReader1.WriteTo(writer)
		return err
	}
	for {
		if line1 < line2 {
			writer.WriteString(line1)
			line1, err = bufReader1.ReadString(LF)
			if err != nil {
				writer.WriteString(line2)
				_, err = bufReader2.WriteTo(writer)
				return err
			}
		} else {
			writer.WriteString(line2)
			line2, err = bufReader2.ReadString(LF)
			if err != nil {
				writer.WriteString(line1)
				_, err = bufReader1.WriteTo(writer)
				return err
			}
		}
	}
}

func mergeBlocks(blockCount int, inputFile string) (int, error) {
	currentIteration := 0
	leftBlocks := blockCount
	for leftBlocks > 1 { //log2(leftBlocks) iterations
		fmt.Println("leftBlocks", leftBlocks)
		for i := 0; i < leftBlocks; i += 2 {
			//only one is remaining, just rename it to next iteration
			if i+1 == leftBlocks {
				err := os.Rename(getBlockFilename(inputFile, i, currentIteration),
					getBlockFilename(inputFile, i/2, currentIteration+1))
				if err != nil {
					return currentIteration, err
				}
				break
			}

			err := mergeFiles(
				getBlockFilename(inputFile, i, currentIteration),
				getBlockFilename(inputFile, i+1, currentIteration),
				getBlockFilename(inputFile, i/2, currentIteration+1))
			if err != nil {
				return currentIteration, err
			}
		}
		leftBlocks = (leftBlocks / 2) + (leftBlocks % 2)
		currentIteration++
	}
	return currentIteration, nil
}

func splitAndSort(inputFile string, maxSizePerBlock int64) (int, error) {
	reader, err := os.Open(inputFile)

	if err != nil {
		return 0, err
	}
	defer reader.Close()

	bufreader := bufio.NewReader(reader)
	blockBuffer := make([]string, 0, preparedBufferSize)
	var currentSize int64
	currentBlock := 0
	for {
		line, err := bufreader.ReadString('\n')
		if err == io.EOF {
			fmt.Println("reading done")
			break
		} else if err != nil {
			return currentBlock, err
		}
		blockBuffer = append(blockBuffer, line)
		currentSize += int64(len(line))
		if currentSize >= maxSizePerBlock {
			currentSize = 0
			if err := sortAndWrite(inputFile, currentBlock, blockBuffer); err != nil {
				return currentBlock, err
			}
			blockBuffer = blockBuffer[:0]
			runtime.GC()
			currentBlock++
		}
	}
	if len(blockBuffer) > 0 {
		if err := sortAndWrite(inputFile, currentBlock, blockBuffer); err != nil {
			return currentBlock, err
		}
		currentBlock++
	}
	//try to free memory
	blockBuffer = []string{}
	runtime.GC()
	return currentBlock, nil
}

func main() {
	outputFile := flag.String("o", "sorted.txt", "output with sorted data")
	inputFile := flag.String("i", "output.txt", "input text file")
	maxSizePerBlock := flag.Int64("size", 1*GB, "max size to read per once")
	flag.Parse()

	start := time.Now()

	currentBlocksCount, err := splitAndSort(*inputFile, *maxSizePerBlock)
	if err != nil {
		panic(err)
	}
	itNumber, err := mergeBlocks(currentBlocksCount, *inputFile)
	if err != nil {
		panic(err)
	}
	//don't check it
	os.Remove(*outputFile)
	err = os.Rename(getBlockFilename(*inputFile, 0, itNumber), *outputFile)
	if err != nil {
		panic(err)
	}
	fmt.Println("total time:", time.Since(start))
}
