package main

import (
	"flag"
	"fmt"
	"io"
	"os"
	"sort"
	"time"
)

const preparedBufferSize = 1000

type StringSlice []string

func readBlock(reader *os.File, maxLines int64, lineSize int64, blockBuffer []string) (int64, []string, error) {
	var currentLine int64
	lineBuffer := make([]byte, lineSize+1)
	for {
		_, err := reader.Read(lineBuffer)
		if err == io.EOF {
			fmt.Println("reading done")
			return currentLine, blockBuffer, io.EOF
		} else if err != nil {
			return currentLine, blockBuffer, err
		}
		blockBuffer = append(blockBuffer, string(lineBuffer))
		currentLine++
		if currentLine >= maxLines {
			return currentLine, blockBuffer, nil
		}
	}
	return 0, nil, nil
}

func writeBuffer(file *os.File, buff []string) {
	for _, str := range buff {
		file.WriteString(str)
	}
}

func main() {
	inputFile := flag.String("i", "output.txt", "input text file")
	maxLinesPerBlock := flag.Int64("max", 10000, "max lines to read per once")
	lineSize := flag.Int64("length", 10, "line length without trailing \\n")
	flag.Parse()

	start := time.Now()

	file, err := os.OpenFile(*inputFile, os.O_RDWR, 0755)
	if err != nil {
		panic(err)
	}
	*maxLinesPerBlock = *maxLinesPerBlock / 2
	lines := make([]string, 0, preparedBufferSize)
	for {
		iterationsNumber := 0
		savedPos, _ := file.Seek(0, 1)
		linesCount, b, err := readBlock(file, *maxLinesPerBlock, *lineSize, lines)
		lines = b
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
		iterationsNumber++
		for {
			nextLinesCount, b, err := readBlock(file, *maxLinesPerBlock, *lineSize, lines)
			if nextLinesCount == 0 {
				break
			}
			lines = b
			if err != nil && err != io.EOF {
				panic(err)
			}
			sort.Stable(sort.StringSlice(lines))
			file.Seek(-nextLinesCount*(*lineSize+1), 1)
			writeBuffer(file, lines[linesCount:])
			lines = lines[0:linesCount]
			if io.EOF == err {
				break
			}
			iterationsNumber++
		}
		file.Seek(savedPos, 0)
		writeBuffer(file, lines[0:linesCount])
		lines = lines[:0]
		fmt.Println("iterations number", iterationsNumber)
	}

	fmt.Println("total time:", time.Since(start))
}
